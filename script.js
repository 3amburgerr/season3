// setInterval(function() {
//   location.reload();
// }, 60000);

// Get the grid container element
const container = document.querySelector('.grid-container');

// Get the number of days in a year
const daysInYear = 365;

// Loop through each row of the grid
for (let row = 0; row < 10; row++) {
  // Loop through each column of the grid
  for (let col = 0; col < 37; col++) {
    // Calculate the day number based on the row and column
    const dayNumber = col * 10 + row + 1;
    
    // Create a new grid item element
    const item = document.createElement('div');
    item.classList.add('grid-item');
    item.classList.add('day');
    
    // Add a unique class name for the date value (e.g. "day-2022-04-17")
    const date = new Date(2023, 0, dayNumber); // Jan 1, 2023 is a Sunday (day 0)
    const className = `day-${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
    item.classList.add(className);
    
    // Add the day number as a data attribute
    item.dataset.date = date.toDateString();
    
    // Add the grid item to the container
    container.appendChild(item);
  }
}

// Make an API call to retrieve the data from Google Sheets
fetch('https://sheets.googleapis.com/v4/spreadsheets/1nxT2LH5TuvJmExz_C2GPdDBnLVt6DIX2clY_r0h1F38/values/test!A2:AZO20', {
  headers: {
    'Authorization': 'Bearer ya29.a0Ael9sCNq1mOVTjGQiZx337KwacIEcznFzvDRqtY1c5aRgGAj3KASKFDzpTv5SPKJFJKcMUkrj11oePrj_tTt3K9BesEsq6JoPeDgWhB-lr-x1adrh8ZVtfIQhVH4Q5qQ9uWbScrBkpN9Xewle1wmoYYuDkthme8aCgYKAVMSARISFQF4udJhAqkfwaISMrs40-2omGosgQ0166',
    'Content-Type': 'application/json'
  }
})
  .then(response => response.json())
  .then(data => {
    // Parse the data and extract the values from the first row
    const values = data.values[0];
    
    // Loop through each grid item and update the background color based on the corresponding value
    const gridItems = document.querySelectorAll('.grid-item');
    gridItems.forEach((item, index) => {
      if (values[index] === 'green') {
        item.style.backgroundColor = 'pink';
      } 
      // else if (values[index] === 'red') {
      //   item.style.backgroundColor = 'red';
      // }
    });
  })
  .catch(error => console.error(error));

// Get all grid items
const gridItems = document.querySelectorAll('.grid-item');

// Loop through the grid items and add event listener
gridItems.forEach((item) => {
  item.addEventListener('mouseover', () => {
    // Get the date represented by the grid item
    const date = item.dataset.date;
    //console.log(date)
    // Set the tooltip text to the date
    item.setAttribute('data-tooltip', date);
  });
});
