//reference: https://www.youtube.com/watch?v=MiPpQzW_ya0

const {google} = require('googleapis');
const keys = require('./keys.json')







const client = new google.auth.JWT(
  keys.client_email, 
  null, 
  keys.private_key, 
  ['https://www.googleapis.com/auth/spreadsheets']
);


client.authorize(function(err,tokens){

  if(err){
    console.log(err);
    return;
  } else {
    console.log('Connected!');
  }

})


async function gsrun(cl){

  const gsapi = google.sheets({version:'v4', auth: cl});

  const opt = {
    spreadsheetId: '1nxT2LH5TuvJmExz_C2GPdDBnLVt6DIX2clY_r0h1F38',
    range: 'test!A2:AZO20'
  };

  let data = await gsapi.spreadsheets.values.get(opt);
  let dataArray = data.data.values;



  dataArray = dataArray.map(function(r){
   while (r.length < 2){
    r.push('');
   }
   return r;
  })


  

  let newDataArray = dataArray.map(function(r){
    r.push(r[0] + '-' + r[1]);
    return r;
  });


  const updateOptions = {
    spreadsheetId: '1nxT2LH5TuvJmExz_C2GPdDBnLVt6DIX2clY_r0h1F38',
    range: 'Sheet2!A2',
    valueInputOption: 'USER_ENTERED',
    resource: { values: newDataArray}
  };

  let res = await gsapi.spreadsheets.values.update(updateOptions)

  
  // console.log(res);

  console.log(newDataArray);

}

gsrun(client);